import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { MapaPage } from '../pages/mapa/mapa';
import { CapturaPage } from '../pages/captura/captura';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core/services/google-maps-api-wrapper';
import { DomicilioService } from '../services/domicilioService';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    MapaPage,
    CapturaPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyC5DkEDsylPfwija_Ps1GWfFm9cO87BbXo',
    libraries: ['places']})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    MapaPage,
    CapturaPage
  ],
  providers: [GoogleMapsAPIWrapper, 
      {provide: ErrorHandler, useClass: IonicErrorHandler}, 
      {provide:DomicilioService,useClass:DomicilioService}]
})
export class AppModule {}
