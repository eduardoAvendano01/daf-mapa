import { Component } from '@angular/core';

import { NavController, AlertController } from 'ionic-angular';
import { MapsAPILoader } from 'angular2-google-maps/core';
import { Geolocation } from 'ionic-native';
import { DomicilioService } from '../../services/domicilioService';
import { Platform } from 'ionic-angular';

declare var google:any;

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html'
})
export class MapaPage {

  map: any;
  componentForm: any;
  showMarker: boolean;

  codigoPostal: string;
  nombreCalle: string;
  numeroCalle: string;
  estado: string;
  ciudad: string;
  colonia: string;
  delegacion: string;
  numeroInterior: string;
  isBrowser: boolean;
  
  

  constructor(public platform: Platform, public navCtrl: NavController, private  _loader : MapsAPILoader, private domicilioForm: DomicilioService, public alertCtrl: AlertController) {

    //Checar si es browser o device
    if(this.platform.is('core') || this.platform.is('mobileweb')) {
      console.log("Its not an app");
      this.isBrowser = false;
    } else {
      console.log("Its an an app");
      this.isBrowser = true;
    }

      this.showMarker = false;
      

      this.componentForm = {
        postal_code: 'short_name', //codigo postal
        route: 'long_name', //nombre calle
        street_number: 'short_name', //numero calle
        administrative_area_level_1: 'long_name', //Estado
        locality: 'long_name', //Ciudad
        sublocality_level_1: 'short_name', //Colonia
        administrative_area_level_3: 'short_name' //Delegacion
      };

      this.map = {
            lat: domicilioForm.latitudMapa, 
            lng: domicilioForm.longitudMapa,
            zoom: 15, 
            disableUI: true,
            zoomControl: false,
            streetViewControl: false
        };

        this.codigoPostal = domicilioForm.postal_code;
        this.nombreCalle = domicilioForm.route;
        this.numeroCalle = domicilioForm.street_number;
        this.estado = domicilioForm.administrative_area_level_1;
        this.ciudad = domicilioForm.locality;
        this.colonia = domicilioForm.sublocality_level_1;
        this.delegacion = domicilioForm.administrative_area_level_3;
        this.numeroInterior = domicilioForm.num_int;

        this.getCurrentLocation();
        this.autocomplete();

  }

  getCurrentLocation(){
    Geolocation.getCurrentPosition().then((position) => {

          this.domicilioForm.geolocationLatitude = position.coords.latitude;
          this.domicilioForm.geolocationLongitude = position.coords.longitude;

          this.map = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
            zoom: 15, 
            disableUI: true,
            zoomControl: false,
            streetViewControl: false
          };
                
            
        }, (err) => {
          console.log("Error al obtener geolocalizacion "+err);

        });
  }

  autocomplete() {
        this._loader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete( document.getElementById('autocomplete').getElementsByTagName('input')[0], {componentRestrictions: {country: "mx"}});
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                console.log("En listener")
                let place = autocomplete.getPlace();
                this.map.lat  = place.geometry.location.lat();
                this.map.lng = place.geometry.location.lng();

                //Actualiza service domicilioForm
                this.domicilioForm.latitudMapa = place.geometry.location.lat();
                this.domicilioForm.longitudMapa = place.geometry.location.lng();

                //console.log(place);

                this.showMarker=true;
                this.domicilioForm.showMarker = true;              

                this.domicilioForm.postal_code = '';
                this.domicilioForm.route = '';
                this.domicilioForm.street_number = '';
                this.domicilioForm.administrative_area_level_1 = '';
                this.domicilioForm.locality = '';
                this.domicilioForm.sublocality_level_1 = '';
                this.domicilioForm.administrative_area_level_3 = '';
                this.domicilioForm.num_int = '';

                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                  var addressType = place.address_components[i].types[0];
                  if (this.componentForm[addressType]) {
                    var addressDetail = place.address_components[i][this.componentForm[addressType]];
                    //console.log("Address detail "+addressDetail+" address type "+addressType);
                    //document.getElementById(addressType).value = val;
                    this.domicilioForm[addressType] = addressDetail;  
                  }
                }

                this.codigoPostal = this.domicilioForm.postal_code;
                this.nombreCalle = this.domicilioForm.route;
                this.numeroCalle = this.domicilioForm.street_number;
                this.estado = this.domicilioForm.administrative_area_level_1;
                this.ciudad = this.domicilioForm.locality;
                this.colonia = this.domicilioForm.sublocality_level_1;
                this.delegacion = this.domicilioForm.administrative_area_level_3;
                this.numeroInterior = this.domicilioForm.num_int;



                console.log(this.domicilioForm);
            }); 
        });
    }


    mostrarAlertaNumero(){

      console.log("En alerta numero interior");
      
      let alert = this.alertCtrl.create();
      alert.setTitle('Número Interior');
      alert.setSubTitle('Si tiene un número interior ingréselo, de otro modo sólo presione siguiente.');

      alert.addInput({
        type: 'input',
        label: 'Numero Interior:',
        value: '',
      });

      
      alert.addButton({
        text: 'Siguiente',
        handler: data => {
          console.log(' data:');
          console.log(data);

          this.domicilioForm.num_int = data[0];
          this.navCtrl.parent.select(1);
        }
      });


      alert.present();
    }
  


}