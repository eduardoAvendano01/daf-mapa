import { Component, ViewChild } from '@angular/core';

import { NavController,Tabs } from 'ionic-angular';
import { DomicilioService } from '../../services/domicilioService';

import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { MapaPage } from '../mapa/mapa';
import { CapturaPage } from '../captura/captura';
import {App} from "ionic-angular";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('myTabs') tabRef: Tabs;
  
  tab1Root: any = HomePage;
  tab2Root: any = AboutPage;
  tab3Root: any = ContactPage;
  tabMapaRoot: any = MapaPage;
  tabCapturaRoot: any = CapturaPage;

  constructor(public navCtrl: NavController, public app: App, private domicilioForm: DomicilioService) {
    
  }

  capturaPageSelected(){
    console.log("On capturaPageSelected method");
    this.domicilioForm.fireTabSelectedEvent();  
  }
}
