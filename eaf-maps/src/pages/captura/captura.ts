import { Component } from '@angular/core';

import { NavController, Events } from 'ionic-angular';
//import { MapsAPILoader } from 'angular2-google-maps/core';
import { DomicilioService } from '../../services/domicilioService';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';

declare var google:any;

@Component({
  selector: 'page-captura',
  templateUrl: 'captura.html'
})
export class CapturaPage {

  map: any;
  isBrowser: boolean;
  showMarker: boolean;

  codigoPostal: string;
  nombreCalle: string;
  numeroCalle: string;
  estado: string;
  ciudad: string;
  colonia: string;
  delegacion: string;
  numeroInterior: string;

  domicilios: any;
  estados: any;

  baseURLFindByCP: string = 'http://192.168.0.115:8080/o/allianz-ws/catalogo.sepomex/findSepomexByCodigo';
  baseURLFindEstados: string = 'http://azmexliferay2.eastus2.cloudapp.azure.com:8080/o/allianz-ws/catalogos.estados/findEstados';


  constructor(public platform: Platform, public navCtrl: NavController, private domicilioForm: DomicilioService, public events: Events, public http: Http) {

      //Checar si es browser o device
      if(this.platform.is('core') || this.platform.is('mobileweb')) {
        console.log("Is not an app");
        this.isBrowser = false;
      } else {
        console.log("Is an an app");
        this.isBrowser = true;
      }



      console.log("Domicilio service en Captura ");
      console.log(domicilioForm);
      this.codigoPostal = domicilioForm.postal_code;
      this.nombreCalle = domicilioForm.route;
      this.numeroCalle = domicilioForm.street_number;
      this.estado = domicilioForm.administrative_area_level_1;
      this.ciudad = domicilioForm.locality;
      this.colonia = domicilioForm.sublocality_level_1;
      this.delegacion = domicilioForm.administrative_area_level_3;
      this.numeroInterior = domicilioForm.num_int;

      
      events.subscribe('tabSelected', () => {
        console.log('On tabSelected listener');
        this.codigoPostal = domicilioForm.postal_code;
        this.nombreCalle = domicilioForm.route;
        this.numeroCalle = domicilioForm.street_number;
        this.estado = domicilioForm.administrative_area_level_1;
        this.ciudad = domicilioForm.locality;
        this.colonia = domicilioForm.sublocality_level_1;
        this.delegacion = domicilioForm.administrative_area_level_3;
        this.numeroInterior = domicilioForm.num_int;


        this.showMarker = domicilioForm.showMarker;

      if(domicilioForm.geolocationLatitude!== 0 && domicilioForm.geolocationLongitude !==0){
        console.log("En if");
        this.map = {
            lat: this.domicilioForm.latitudMapa,
            lng: this.domicilioForm.longitudMapa,
            zoom: 15, 
            disableUI: true,
            zoomControl: false,
            streetViewControl: false
        };
      }else{
        console.log("En else");
        this.map = {
            lat: domicilioForm.latitudMapa, //cambiar
            lng: domicilioForm.longitudMapa,
            zoom: 15, 
            disableUI: true,
            zoomControl: false,
            streetViewControl: false
        };
      }
        
      });

      this.showMarker = domicilioForm.showMarker;

      if(domicilioForm.geolocationLatitude!== 0 && domicilioForm.geolocationLongitude !==0){
        this.map = {
            lat: domicilioForm.geolocationLatitude,
            lng: domicilioForm.geolocationLongitude,
            zoom: 15, 
            disableUI: true,
            zoomControl: false,
            streetViewControl: false
        };
      }else{
        this.map = {
            lat: domicilioForm.latitudMapa,
            lng: domicilioForm.longitudMapa,
            zoom: 15, 
            disableUI: true,
            zoomControl: false,
            streetViewControl: false
        };
      }

      

       /* 
      if( this.codigoPostal !== '' && this.codigoPostal !== null){
        console.log("En if codigo postal");
        console.log("Codigo postal "+this.domicilioForm.postal_code);

        this.http.get(this.baseURLFindByCP+"?codigo="+this.domicilioForm.postal_code).map(res => res.json()).subscribe(data => {
          console.log(data[0].asentamiento);

          this.domicilios = data;
        });
      }else{
          console.log("En else");

          this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
            console.log(data[0].descripcion);

            this.estados = data;
          });
      }*/

      this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
        console.log(data[0].descripcion);

        this.estados = data;
      });
      

      
  }

}